--version reportType
1. Checked out from dev_staff.
2. SubmitReportType model added.

--version dataroom
1. Checked out from dev_staff
2. DataRoom changes added.

--version dev_staff
1. Appconfig.js file is added.
2. Room priviledge api added.
3. Multer is added for dataroom file upload.


-- version activity-reg
1. Checked out from dev-staff branch
2. This branch contains the actvity-reg model (CRUD) operation
3. Check the api's in 'activityRegController' ActivityRegister Routers.
e.g /api/activityReg/list.