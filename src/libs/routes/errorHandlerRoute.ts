import { NextFunction, Request, Response } from 'express';

export default (returnError: boolean) => (err, req: Request, res: Response, next: NextFunction) => {
  const { status, error, ...rest } = err;
  const code = status || 500;
  return res.status(code).send({
    ...(returnError && { error: error || [] }),
    status: code,
    ...rest
  });
};
