

class SystemResponse {
  static success(message: string, data: any) {
    return ({
      message,
      data,
      timestamp: new Date().getTime(),
      status: 200
    })
  }

  static badRequestError(message: string = 'BAD REQUEST', error: any = {}) {
    return ({
      message,
      error,
      timestamp: new Date().getTime(),
      status: 422
    })
  }

  static notFoundError(message: string = 'NOT FOUND', error: any = {}) {
    return ({
      message,
      error,
      timestamp: new Date().getTime(),
      status: 404
    })
  }

  static unauthorizedError(message: string = 'UNAUTHORIZED', error: any = {}) {
    return ({
      message,
      error,
      timestamp: new Date().getTime(),
      status: 401
    })
  }

  static internalServerError(error) {
    return ({
      message: 'INTERNAL SERVER ERROR',
      error,
      timestamp: new Date().getTime(),
      status: 500
    })
  }
}


export default SystemResponse;