import * as regex from './regularExpressions';
export const message = {
  invalid: (key) => regex.stringChecker.test(key) ? `${key} are invalid` : `${key} is invalid`,
  required: (key) =>  regex.stringChecker.test(key) ? `${key} are required` : `${key} is required`,
};
