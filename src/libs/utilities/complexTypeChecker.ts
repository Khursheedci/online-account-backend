export const isEmpty = (value, error) => {
  if (!value) {
    throw new Error(error);
  }
};

export const isNumber = (value, error) => {
  if (typeof value === 'number') {
    throw new Error(error);
  }
};

export const isNotArray = (value, error) => {
  if (!Array.isArray(value)) {
    throw new Error(error);
  }
};

export const isObject = (value, error) => {
  if (typeof (value) === 'object') {
    throw new Error(error);
  }
};
