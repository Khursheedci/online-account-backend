export const API_PREFIX = '/api';

// Listing of Environments
export enum EnvVars {
  TEST = 'test',
  LOCAL = 'local',
  DEV = 'dev',
  STG = 'stg',
  PROD = 'prod',
}

export const YES_NO = ['yes', 'no'];
export const DRAFT_FINAL = [1, 2]

export const ERROR_MESSAGES = {
};

export const SUCCESS_MESSAGES = {
};

export const API_METHOD = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE'
};


export const defaultQuery = {
  deletedAt: { $exists: false },
};
