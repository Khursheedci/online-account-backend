import { NextFunction, Request, Response } from 'express';

import { matchedData } from 'express-validator/filter';
import * as _ from 'lodash';
// import { CacheService } from '../services';

export const filterViaSchema = (req, res, next) => {
  req.body = { ...matchedData(req, { locations: ['body', 'query', 'params'] }) };
  next();
};

// export const cache = async (req, res, next) => {
//   const { originalUrl, url } = req;
//   const key = `__express__${originalUrl || url}`;

//   const result = CacheService.get(key);
//   if (result) {
//     res.locals.cache = result;
//     next();
//   } else {
//     res.sendResponse = res.send;
//     res.send = async (body) => {
//       const { data } = body;
//       if (data) { CacheService.set(key, data); }
//       res.sendResponse(body);
//     };
//     delete res.locals.cache;
//     next();
//   }
// };

export const checkAuthorization = (req: Request, res: Response, next: NextFunction) => {
  const { accessRights } = res.locals;
  const code = accessRights.map((clientParam) => clientParam.split(':')[3]);
  const clientCodes = accessRights.map((accessRight: string) => (
    accessRight.split(':')[1]
  )).filter((accessRight: string) => accessRight);

  const mappedAccessRights = accessRights.map((accessRight: string) => {
    const [, client, , resource] = accessRight.split(':');
    return { client, resource };
  });

  res.locals.access = code;
  res.locals.clientCodes = clientCodes;
  res.locals.mappedAccessRights = mappedAccessRights;
  next();
};
