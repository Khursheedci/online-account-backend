import AuthManager from "../../middlewares/AuthManager";
import { checkSchema } from "express-validator";
import { Router } from "express";

import kpiTargetController from "./controller";
import {
  registerUserValidation,
  deleteUserValidations,
  updateUserValidation,
} from "./validation";
import { checkValidation } from "../../middlewares";

const authManager: AuthManager = AuthManager.getInstance(2);
const auth: any = authManager.auth;
const router = Router();

router
  .route("/register")
  .post(
    auth,
    checkSchema(registerUserValidation),
    checkValidation,
    kpiTargetController.register
  );

router.route('/list')
  .get(
    auth,
    kpiTargetController.list
)
router.route('/:userId')
  .delete(
    auth,
    checkSchema(deleteUserValidations),
    checkValidation,
    kpiTargetController.delete,
  )
  .put(
    auth,
    checkSchema(updateUserValidation),
    checkValidation,
    kpiTargetController.update,
  );
export default router;
