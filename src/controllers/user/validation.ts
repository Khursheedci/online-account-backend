import { Schema } from "express-validator";

export const registerUserValidation: Schema = {
  firstName: {
    in: ["body"],
    errorMessage: "Name is not provided",
    isAlpha: {
      errorMessage: "Name can only contain alphabets with no space"
    }
  },
  lastName: {
    in: ["body"],
    errorMessage: "Last Name is not valid or not provided",
    isAlpha: {
      errorMessage: "Name can only contain alphabets with no space"
    },
  },
  email: {
    in: ["body"],
    errorMessage: "Email is required",
    isEmail: true,
  },

  phoneNumber: {
    in: ["body"],
    errorMessage: "Mobile Number is not valid",
    isNumeric: true,
    isLength: {
      errorMessage: 'PhoneNumber should be of 6 digits',
        // Multiple options would be expressed as an array
        options: { min: 6, max: 6 }
    }
  },

  dataRoomId: {
    in: ["body"],
    isArray: true,
    errorMessage: "This field should be an string ",
  },

   
  reportPrivlege: {
    in: ["body"],
    isNumeric: true,
    errorMessage: 'Room Privilege needs needs to be number',
  },

   supervisor: {
    in: ["body"],
    isArray: true,
    errorMessage: "This field  is required",
  },
      'supervisor.*': {
    isMongoId: true,
    errorMessage: ' Member Id needs to be valid id'
  },
  KPIPrivlege: {
    in: ["body"],
    isNumeric: true,
    errorMessage: 'Room Privilege needs needs to be number',
  },
  password: {
    in: ["body"],
    isString: true,
    isLength: {
      options: { max: 30, min: 6 },
      errorMessage: 'Password needs to be of 6 to 30 characters'
    },
    optional: true
  },

  agreementPrivileges: {
    in: ["body"],
    isBoolean: true,
    errorMessage: 'Agreement Privilege  needs to be Boolean'
    
  }


};

export const deleteUserValidations: Schema = {
  userId: {
    in: ['params'],
    isMongoId: true,
    errorMessage: 'Invalid userId Provided',
  }
}

export const updateUserValidation: Schema = {
  ...registerUserValidation,
  ...deleteUserValidations,
}

export const loginValidations: Schema = {
  email: {
    in: ["body"],
    errorMessage: "Email is required",
    isEmail: true,
  },
  password: {
    in: ["body"],
    isString: true,
    errorMessage: 'Password is required',
    isLength: {
      errorMessage: 'Password needs to be minimum of 6 characters',
      options: { min: 6},
    }
  }
}