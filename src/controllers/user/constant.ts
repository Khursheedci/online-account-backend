const ResultMessages = {
  USERS_FETCH_SUCCESS: 'Fetched successfully.',
  USER_FETCH_SUCCESS : 'Fetched successfully.',
  DATA_NOT_FOUND : 'Data not found',
  REMOVED_USER: 'Successfully removed vendor agreement',
  UNABLE_TO_REMOVE: 'Unable to remove vendor agreement',
  USER_ALREADY_EXIST: 'An user already exists with the email',
  USER_CREATED_SUCCESS: 'User created Successfully',
  USER_LOGIN_SUCCESS: 'User logged successfully',
  USERS_LOGIN_FAILED: 'No matching user for provided email and password',
  UNABLE_FETCH_ROOM_PRIVILEGE: 'Unable to find data room privilege'
};
export default ResultMessages;
