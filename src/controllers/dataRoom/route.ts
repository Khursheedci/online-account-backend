import AuthManager from "../../middlewares/AuthManager";
import { checkSchema } from "express-validator";
import { Router } from "express";
import * as multer from "multer";

import dataRoomController from "./controller";
import {
  registerUserValidation,
  deleteUserValidations,
  updateUserValidation,
} from "./validation";
import { checkValidation } from "../../middlewares";
import DataRoomUploadSercie from '../../services/uploadService';

const authManager: AuthManager = AuthManager.getInstance(2);
const auth: any = authManager.auth;
const router = Router();
const upload = multer({
  limits: {
    fileSize: 1024 * 1024 * 5
  },
});


router
  .route("/register")
  .post(
    auth,
    upload.single('DataRoomFile'),
    checkSchema(registerUserValidation),
    checkValidation,
    dataRoomController.register
  );

router.route('/list')
  .get(
    auth,
    dataRoomController.list
  )
router.route('/:userId')
  .delete(
    auth,
    checkSchema(deleteUserValidations),
    checkValidation,
    dataRoomController.delete,
  )
  .put(
    auth,
    upload.single('DataRoomFile'),
    checkSchema(updateUserValidation),
    checkValidation,
    dataRoomController.update,
  );
export default router;
