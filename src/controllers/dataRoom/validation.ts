import { Schema } from "express-validator";

export const registerUserValidation: Schema = {

  dataRoomId: {
    in: ["body"],
    errorMessage: "Data Room Id is not valid or not provided",
    isNumeric: {
      errorMessage: "This field cannot be empty"
    },
  },

  dataRoomPriviledgeId: {
    in: ["body"],
    errorMessage: " Data Room Privilege Id is not valid or not provided",
    isNumeric: {
      errorMessage: "This field cannot be empty"
    },
  },

  fileName: {
    in: ["body"],
    errorMessage: "File Name is not valid or not provided",
    isString: {
      errorMessage: "This field cannot be empty"
    },
  },
  //  fileId: {
  //   in: ["body"],
  //   errorMessage: "File ID is not valid or not provided",
  //   isString: {
  //     errorMessage: "This field cannot be empty"
  //   },
  // },
  //  filePath: {
  //   in: ["body"],
  //   errorMessage: "File Path is not valid or not provided",
  //   isString: {
  //     errorMessage: "This field cannot be empty"
  //   },
  // },

  userId: {
    in: ["body"],
    errorMessage: "User Id is not valid or not provided",
    isString: {
      errorMessage: "This field cannot be empty"
    },
  },
};

export const deleteUserValidations: Schema = {
  userId: {
    in: ['params'],
    isMongoId: true,
    errorMessage: 'Invalid userId Provided',
  }
}

export const updateUserValidation: Schema = {
  ...registerUserValidation,
  ...deleteUserValidations,
}

export const loginValidations: Schema = {
  email: {
    in: ["body"],
    errorMessage: "Email is required",
    isEmail: true,
  },
  password: {
    in: ["body"],
    isString: true,
    errorMessage: 'Password is required',
    isLength: {
      errorMessage: 'Password needs to be minimum of 6 characters',
      options: { min: 6 },
    }
  }
}