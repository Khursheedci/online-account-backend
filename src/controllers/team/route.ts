import AuthManager from "../../middlewares/AuthManager";
import { checkSchema } from "express-validator";
import { Router } from "express";

import teamController from "./controller";
import {
  registerUserValidation,
  deleteUserValidations,
  updateUserValidation,
} from "./validation";
import { checkValidation } from "../../middlewares";

const authManager: AuthManager = AuthManager.getInstance(2);
const auth: any = authManager.auth;
const router = Router();

router
  .route("/register")
  .post(
    auth,
    checkSchema(registerUserValidation),
    checkValidation,
    teamController.register
  );

router.route('/list')
  .get(
    auth,
    teamController.list
)
router.route('/:userId')
  .delete(
    auth,
    checkSchema(deleteUserValidations),
    checkValidation,
    teamController.delete,
  )
  .put(
    auth,
    checkSchema(updateUserValidation),
    checkValidation,
    teamController.update,
  );
export default router;
