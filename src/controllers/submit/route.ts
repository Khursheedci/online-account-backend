import AuthManager from "../../middlewares/AuthManager";
import { checkSchema } from "express-validator";
import { Router } from "express";

import submitController from "./controller";
import {
  registerUserValidation,
  deleteUserValidations,
  updateUserValidation,
} from "./validation";
import { checkValidation } from "../../middlewares";

const authManager: AuthManager = AuthManager.getInstance(2);
const auth: any = authManager.auth;
const router = Router();
import * as multer from "multer";
const upload = multer({
  limits: {
    fileSize: 1024 * 1024 * 5
  },
});

router
  .route("/register")
  .post(
    auth,
    upload.single('SubmitRoomFile'),
    checkSchema(registerUserValidation),
    checkValidation,
    submitController.register
  );

router.route('/list')
  .get(
    auth,
    submitController.list
)
router.route('/:userId')
  .delete(
    auth,
    checkSchema(deleteUserValidations),
    checkValidation,
    submitController.delete,
  )
  .put(
    auth,
    upload.single('SubmitRoomFile'),
    checkSchema(updateUserValidation),
    checkValidation,
    submitController.update,
  );
export default router;
