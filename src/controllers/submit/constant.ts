const ResultMessages = {
  USERS_FETCH_SUCCESS: 'Fetched successfully.',
  USER_FETCH_SUCCESS : 'Updated successfully.',
  DATA_NOT_FOUND : 'Data not found',
  REMOVED_USER: 'Successfully removed vendor agreement',
  UNABLE_TO_REMOVE: 'Unable to remove vendor agreement',
  USER_ALREADY_EXIST: 'An staff already exists with the email',
  USER_CREATED_SUCCESS: 'Report submitted Successfully',
  USER_LOGIN_SUCCESS: 'Staff logged successfully',
  USERS_LOGIN_FAILED: 'No matching staff for provided email and password',
  UNABLE_TO_ADD_RECORD: 'Unable to add report',
  UNABLE_TO_UPDATE: 'Unable to update the record'
};
export default ResultMessages;
