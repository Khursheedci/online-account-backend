import { NextFunction, Request, Response } from "express";

import { KpiService } from "../../services";
import SystemResponse from "../../libs/SystemResponse";
import { passwordMethods } from "../../libs/utilities";

import ResultMessages from "./constant";
import logger from "../../libs/Logger";

class KpiController {
  private kpiService: KpiService;

  constructor() {
    this.kpiService = new KpiService();
  }

  public register = async (req, res: Response, next: NextFunction) => {
   
    try {
      const { body } = req;

      await this.kpiService.create(body );

      return res.send(
        SystemResponse.success(ResultMessages.USER_CREATED_SUCCESS, {})
      );
    } catch (err) {
      logger.error(":::::", err);
      return next(err);
    }
  };

  list = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { skip = 0, limit = 0 },
      } = req;
      const kpiList = await this.kpiService.getKpiList({ skip, limit });

      res.send(
        SystemResponse.success(ResultMessages.USERS_FETCH_SUCCESS, kpiList)
      );
    } catch (err) {
      return next(err);
    }
  };

  public delete = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { userId } = req.params;
      const result = await this.kpiService.delete(userId);

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.REMOVED_USER, result)
      );
    } catch (err) {
      return next(err);
    }
  };

  public update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { userId },
        body,
      } = req;
      const result = await this.kpiService.update({
        ...body,
        originalId: userId,
      });

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.USER_FETCH_SUCCESS, result)
      );
    } catch (err) {
      return next(err);
    }
  };
}

export default new KpiController();
