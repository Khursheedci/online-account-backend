import { Schema } from "express-validator";

export const registerUserValidation: Schema = {
kpiName: {
    in: ["body"],
    errorMessage: "Name is not provided",
    isString: {
      errorMessage: "Kpi Name can only contain alphabets with no space"
    }
  },

  reportStartDate: {
    in: ["body"],
    isDate: true,
    errorMessage: "This field is required",
  },

 
   reportPeriod: {
    in: ["body"],
     isNumeric: true,
    errorMessage: "This field is required",
  },
   
  metric : {
    in: ["body"],
    isNumeric: true,
    errorMessage: "This field is required",
  },

 

description: {
    in: ["body"],
    errorMessage: "Description is not valid or not provided",
    isString: {
      errorMessage: "Name can only contain alphabets with no space"
    },
  },
  
};

export const deleteUserValidations: Schema = {
  userId: {
    in: ['params'],
    isMongoId: true,
    errorMessage: 'Invalid userId Provided',
  }
}

export const updateUserValidation: Schema = {
  ...registerUserValidation,
  ...deleteUserValidations,
}

export const loginValidations: Schema = {
  email: {
    in: ["body"],
    errorMessage: "Email is required",
    isEmail: true,
  },
  password: {
    in: ["body"],
    isString: true,
    errorMessage: 'Password is required',
    isLength: {
      errorMessage: 'Password needs to be minimum of 6 characters',
      options: { min: 6},
    }
  }
}