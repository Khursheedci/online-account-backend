const ResultMessages = {
  USERS_FETCH_SUCCESS: 'Fetched successfully.',
  USER_FETCH_SUCCESS : 'Fetched successfully.',
  DATA_NOT_FOUND : 'Data not found',
  REMOVED_USER: 'Successfully updated vendor agreement',
  UNABLE_TO_REMOVE: 'Unable to remove vendor agreement',
  USER_ALREADY_EXIST: 'An staff already exists with the email',
  USER_CREATED_SUCCESS: 'Activity Registered Successfully'
};
export default ResultMessages;
