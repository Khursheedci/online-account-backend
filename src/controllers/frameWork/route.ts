import AuthManager from "../../middlewares/AuthManager";
import { checkSchema } from "express-validator";
import { Router } from "express";

import frameController from "./controller";
import {
  registerUserValidation,
  deleteUserValidations,
  updateUserValidation,
} from "./validation";
import { checkValidation } from "../../middlewares";

const authManager: AuthManager = AuthManager.getInstance(2);
const auth: any = authManager.auth;
const router = Router();

router
  .route("/register")
  .post(
    auth,
    checkSchema(registerUserValidation),
    checkValidation,
    frameController.register
  );

router.route('/list')
  .get(
    auth,
    frameController.list
)
router.route('/:userId')
  .delete(
    auth,
    checkSchema(deleteUserValidations),
    checkValidation,
    frameController.delete,
  )
  .put(
    auth,
    checkSchema(updateUserValidation),
    checkValidation,
    frameController.update,
  );
export default router;
