// tslint:disable:no-null-keyword
import  logger from '../../libs/Logger';
import * as mongoose from 'mongoose';
import { DocumentQuery } from 'mongoose';
import { IQueryBaseCreate, IQueryBaseCreateAll, IQueryBaseUpdate } from '../entities';

export default class VersioningRepository<D extends mongoose.Document, M extends mongoose.Model<D>> {
  public static generateObjectId() {
    return String(mongoose.Types.ObjectId());
  }

  private modelType: M;

  constructor(modelType) {
    this.modelType = modelType;
  }

  /**
   * Create new application
   * @property {string} body.name - The name of record.
   * @returns {Application}
   */
  public async create(options: IQueryBaseCreate): Promise<D> {
    const id = VersioningRepository.generateObjectId();
    const model = new this.modelType({
      ...options,
      _id: id,
      originalId: id,
    });

    return await model.save();
  }

  public async createAll(options: IQueryBaseCreateAll[]): Promise<D[]> {
    if (options) {
      options.forEach((option) => {
        const id = VersioningRepository.generateObjectId();
        option._id = id;
        option.originalId = id;
      });

      return await this.modelType.insertMany(options);
    }
  }

  /**
   * Create new application
   * @property {string} id - Record unique identifier.
   * @returns {Application}
   */
  public async update(options: IQueryBaseUpdate): Promise<D> {
    logger.debug('Searching for previous valid object...');
    const previous = await this.getById(options.originalId);
    logger.debug('PREVIOUS::::::::', JSON.stringify(previous));

    logger.debug('Invalidating previous valid object...');
    await this.invalidate(options.originalId);

    const updatedAt = Date.now();

    const newInstance: any = {
      ...previous.toJSON(),
      ...options,
      updatedAt,
    };

    newInstance._id = VersioningRepository.generateObjectId();
    logger.debug('NEW INSTANCE::::::::', newInstance);
    delete newInstance.deletedAt;

    const model = new this.modelType(newInstance);

    logger.debug('Creating new object...');
    return await model.save();
  }

  protected getAll(query: any = {}, projection: any = {}, options: any = {}): DocumentQuery<D[], D> {
    options.limit = options.limit || 0;
    options.skip = options.skip || 0;
    query.deletedAt = undefined;
    logger.debug('getAll query: ', query);
    logger.debug('getAll options: ', options);

    return this.modelType.find(query, projection, options);
  }

  protected getByQuery(query: any, projection?: any): DocumentQuery<D | null, D> {
    return this.modelType.findOne({ ...query, deletedAt: null }, projection);
  }

  protected getById(id: string): DocumentQuery<D | null, D> {
    logger.debug(id);
    return this.modelType.findOne({ originalId: id, deletedAt: null });
  }

  protected async remove(id: string): Promise<D> {
    const result = await this.getById(id);
    if (result) {
      return await this.invalidate(id);
    }
    return null;
  }

  protected invalidate(id: string): DocumentQuery<D, D> {
    const now = Date.now();
    return this.modelType.update({ originalId: id, deletedAt: null }, { deletedAt: now });
  }
  
}
