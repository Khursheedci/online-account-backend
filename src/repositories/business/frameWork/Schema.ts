import {
  DRAFT_FINAL
} from '../../../libs/constants';
import VersionableSchema from '../../versionable/VersionableSchema';

export default class FrameWorkSchema extends VersionableSchema {
  constructor(options: any) {
    const baseSchema = {
      frameWorkName: {
        type: String,
        required: true,
      },
      targetType: {
        type:String,
        required: true,
      },
     targetTypeId: {
        type: [String],
        required: true,
      },
      weight:{
        type:Number,
        required: true
      }
    };
    super(baseSchema, options);
  }
}
