import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import IframeWorkModel from './IModel';
import { frameWorkModel } from './model';

class FrameWorkRepository extends VersioningRepository<IframeWorkModel, Model<IframeWorkModel>> {
  constructor() {
    super(frameWorkModel);
  }
  /**
   * Create kpi
   * @returns {Kpi}
   */
  public async create(options: IQueryCreate): Promise<IframeWorkModel> {
    logger.debug('FrameworkRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<IframeWorkModel> {
    logger.debug('FrameworkRepository - delete: ');
    return super.remove(data);
  }

  public async getKpiByQuery(query: any, projection?: any): Promise<IframeWorkModel> {
    logger.debug('Kpi - getKpisByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getKpiById(id: any): Promise<IframeWorkModel> {
    logger.debug('Kpi - getKpiById params: ', id);
    return await super.getById(id).lean();
  }

  public async getKpiList(query, projections, options): Promise<Array<IframeWorkModel>> {
    logger.debug('Kpi - getKpiById params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update kpi
   * @returns {Kpi}
   */
  public async update(options: any): Promise<IframeWorkModel> {
    logger.debug('FrameworkRepository - update: ');
    return super.update(options);
  }
}

export default FrameWorkRepository;
