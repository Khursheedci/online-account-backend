import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import IStaffModel from './IModel';
import { StaffModel } from './model';

class StaffRepository extends VersioningRepository<IStaffModel, Model<IStaffModel>> {
  constructor() {
    super(StaffModel);
  }

  /**
   * Create staff
   * @returns {Staff}
   */
  public async create(options: IQueryCreate): Promise<IStaffModel> {
    logger.debug('StaffRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<IStaffModel> {
    logger.debug('StaffRepository - delete: ');
    return super.remove(data);
  }

  public async getStaffByQuery(query: any, projection?: any): Promise<IStaffModel> {
    logger.debug('Staff - getStaffsByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getStaffById(id: any): Promise<IStaffModel> {
    logger.debug('Staff - getStaffById params: ', id);
    return await super.getById(id).lean();
  }

  public async getStaffList(query, projections, options): Promise<Array<IStaffModel>> {
    logger.debug('Staff - getStaffById params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update staff
   * @returns {Staff}
   */
  public async update(options: any): Promise<IStaffModel> {
    logger.debug('StaffRepository - update: ');
    return super.update(options);
  }
}

export default StaffRepository;
