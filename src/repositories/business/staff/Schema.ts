import {
  DRAFT_FINAL
} from '../../../libs/constants';
import VersionableSchema from '../../versionable/VersionableSchema';

export default class UserSchema extends VersionableSchema {
  constructor(options: any) {
    const baseSchema = {
      firstName: {
        type: String,
        required: true,
      },
      lastName: {
        type: String,
        required: true,
      },
      email: {
        type: String,
        required: true,
      },
      staffId : {
        type: String,
        required: true,
      },
      
      phoneCode: {
        type: String,
        required:true,
      
      },

      phoneNumber: {
        type: String,
        required: true,
      },
      department: {
        type: String,
        required: true,
      },
     
    };
    super(baseSchema, options);
  }
}
