import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IUserModel extends IVersionableDocument {
  id: string;
  firstName: string;
  lastName: string;
  staffId: string;
  email: string;
 phoneCode:number,
  phoneNumber: string;
  department: string; 
}
