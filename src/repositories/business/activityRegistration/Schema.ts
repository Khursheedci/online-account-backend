import {
  DRAFT_FINAL
} from '../../../libs/constants';
import VersionableSchema from '../../versionable/VersionableSchema';

export default class ActivityRegSchema extends VersionableSchema {
  constructor(options: any) {
    const baseSchema = {
      name: {
        type: String,
        required: true,
      },
      description: {
        type: String,
        required: true,
      },
    
      activityById : {
        type: [String],
        required: true
      },
      activityBy: {
        type: Number,
        required: true,
      },

      startDate: {
        type: Date,
        required: true,
      },
      endDate: {
        type: Date,
        required: true,
      },
      isCompleted: {
        type: Boolean,
        default: false,
      },
      milestones: {
        type: [Object]
      },
      
    };
    super(baseSchema, options);
  }
}
