import { DateTimeFormatType } from 'morgan-body';
import IVersionableDocument from '../../versionable/IVersionableDocument';
import SelectBy from './Schema'

export default interface IUserModel extends IVersionableDocument {
  id: string;
  kpiId: [string];
  kpiTarget: number;
  selectBy: string;
  selectById: [string];
  reportingCycle: Date;
  actualKpiTarget: number;
 
}
