import {
  DRAFT_FINAL
} from '../../../libs/constants';
import VersionableSchema from '../../versionable/VersionableSchema';

export default class AgreementSchema extends VersionableSchema {
  constructor(options: any) {
    const baseSchema = {
      contractTitle: {
        type: String,
        required: true,
      },
      party: {
        type: String,
        required: true,
      },
      email: {
        type: String,
        required: true,
      },
      phoneCode: {
        type: String,
        required:true,
      
      },

      phoneNumber: {
        type: String,
        required: true,
      },
      effectiveDate: {
        type: Date,
        required: true,
      },
      expiryDate: {
        type: Date,
        required: true,
      },
       anniversary: {
        type: Date,
        
      }

    };
    super(baseSchema, options);
  }
}


