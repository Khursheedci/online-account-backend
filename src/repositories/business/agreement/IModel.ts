  import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IAgreementModel extends IVersionableDocument {
  id: string;
  
  contractTitle: string;
  party: string;
  email: string;
 phoneCode:number,
  phoneNumber: string;
  effectiveDate: Date;
  expiryDate: Date;
  anniversary: Date;
}
