import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import IKpiModel from './IModel';
import KpiSchema from './Schema';

export const kpiSchema = new KpiSchema({
  collection: 'kpis',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
kpiSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Methods
 */
kpiSchema.method({});

/**
 * Statics
 */
kpiSchema.statics = {};

/**
 * Indexes
 */
kpiSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef Kpi
 */

const KpiModel: mongoose.Model<
  IKpiModel
> = mongoose.model<IKpiModel>(
  'Kpi',
  kpiSchema,
  'kpis',
  true,
);

KpiModel.init();

export { KpiModel };
