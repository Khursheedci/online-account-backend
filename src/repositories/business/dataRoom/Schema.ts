import {
  DRAFT_FINAL
} from '../../../libs/constants';
import VersionableSchema from '../../versionable/VersionableSchema';

export default class ActivityRegSchema extends VersionableSchema {
  constructor(options: any) {
    const baseSchema = {

      userId: {
        type: String,
        required: true,
      },
      dataRoomId: {
        type: Number,
        required: true,
      },
       dataRoomPriviledgeId: {
        type: Number,
        required: true,
      },
     
        fileName: {
        type: String,
        required: true,
      },
      fileId: {
        type: String
      },
      folderPath: {
        type: String
      },
      
    };
    super(baseSchema, options);
  }
}
