import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IUserModel extends IVersionableDocument {

  id: string;
  userId: string;
  dataRoomId: number;
  dataRoomPriviledgeId: number;
  fileName: string;
  fileId: string,
  folderPath:string
  
}
