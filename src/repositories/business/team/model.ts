import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import ITeamModel from './IModel';
import TeamSchema from './Schema';

export const teamSchema = new TeamSchema({
  collection: 'teams',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
teamSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Methods
 */
teamSchema.method({});

/**
 * Statics
 */
teamSchema.statics = {};

/**
 * Indexes
 */
teamSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef Team
 */

const TeamModel: mongoose.Model<
  ITeamModel
> = mongoose.model<ITeamModel>(
  'Team',
  teamSchema,
  'teams',
  true,
);

TeamModel.init();

export { TeamModel };
