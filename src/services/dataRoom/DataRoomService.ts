import { DataRoomRepository } from '../../repositories';

class DataRoomService {
  private dataRoomRepository: DataRoomRepository;

  public constructor() {
    this.dataRoomRepository = new DataRoomRepository();
  }

  public async create(data) {
    return this.dataRoomRepository.create(data);
  }

  public async delete(data) {
    return this.dataRoomRepository.delete(data);
  }

  public async getDataRoomByQuery(query: any): Promise<any> {
    return this.dataRoomRepository.getDataRoomsByQuery(query);
  }
  public async getDataRoomById(id: string): Promise<any> {
    return this.dataRoomRepository.getDataRoomById(id);
  }

  public async getDataRoomList(options: any): Promise<any> {
    return this.dataRoomRepository.getDataRoomList({ isAdmin: { $ne: true } }, { password: 0 }, options);
  }

  public async update(data) {
    return this.dataRoomRepository.update(data);
  }
}

export default DataRoomService;
