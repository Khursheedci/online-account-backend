import { KpiRepository } from '../../repositories';

class KpiService {
  private kpiRepository: KpiRepository;

  public constructor() {
    this.kpiRepository = new KpiRepository();
  }

  public async create(data) {
    return this.kpiRepository.create(data);
  }

  public async delete(data) {
    return this.kpiRepository.delete(data);
  }

  public async getKpiByQuery(query: any): Promise<any> {
    return this.kpiRepository.getKpiByQuery(query);
  }
  public async getKpiById(id: string): Promise<any> {
    return this.kpiRepository.getKpiById(id);
  }

  public async getKpiList(options: any): Promise<any> {
    return this.kpiRepository.getKpiList({ isAdmin: { $ne: true } }, { password: 0 }, options);
  }

  public async update(data) {
    return this.kpiRepository.update(data);
  }
}

export default KpiService;
