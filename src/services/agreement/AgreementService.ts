import { AgreementRepository } from '../../repositories';
import uploadAgreement from '../uploadAgreement'

class AgreementService {
  private agreementRepository: AgreementRepository;

  public constructor() {
    this.agreementRepository = new AgreementRepository();
  }

  public async create(data) {
    return this.agreementRepository.create(data);
  }

  public async delete(data) {
    return this.agreementRepository.delete(data);
  }

  public async getAgreementByQuery(query: any): Promise<any> {
    return this.agreementRepository.getAgreementByQuery(query);
  }
  public async getAgreementById(id: string): Promise<any> {
    return this.agreementRepository.getAgreementById(id);
  }

  public async getAgreementList(options: any): Promise<any> {
    return this.agreementRepository.getAgreementList({ isAdmin: { $ne: true } }, { password: 0 }, options);
  }

  public async update(data) {
    return this.agreementRepository.update(data);
  }

 public async uploadAgreementReport(file, filename) {
    try {
      // const result = await uploadReport(file, filename);
      // return true;
      return await uploadAgreement(file, filename);
    } catch(e) {
      throw {'error': 'Error in uploading file'};
    }
  }
}


export default AgreementService;
