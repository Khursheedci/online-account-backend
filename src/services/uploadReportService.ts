

import * as path from 'path';
import * as fs from 'fs';

const saveToDirectory = async (file, fileName) => {
  // const filePath = getFolderPath(roomId, dataRoomPriviledgeId);
  // const filePath = path.join(__dirname, '../../src/Report')
  const time = Date.now();
  let dirPath = path.join(__dirname, `../../src/Report/${time}_${fileName}`);
  console.log("::::", dirPath)
  return new Promise((resolve, reject) => {
    fs.writeFile(dirPath, file.buffer, {}, (err) => {
      if(err) {
        reject(err);
      }
      resolve({ newFileName: `${fileName}_${time}`, folderPath : dirPath});
    });
  })
}

export default saveToDirectory;