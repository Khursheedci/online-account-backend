import { appConfig } from '../appConfig';
import * as fs from 'fs';
import * as path from 'path';

// const DIR = '../upload/';
// const DIR = path.join(__dirname, '../../src/upload');
// console.log('Current Directory',__dirname);
// console.log('DIR', DIR);

const getFolderPath = (roomId, dataRoomPriviledgeId) => {
  var DataRoomPriviledge = appConfig['DataRoomPrivilege'];
  if (!DataRoomPriviledge) {
    return null;
  } else {
    let ob = DataRoomPriviledge[roomId - 1];
    if (!ob || !ob.cabinetName) {
      return null;
    } else {
      var ob2 = ob.cabinetReports[dataRoomPriviledgeId - 1];
      if (!ob2 || !ob2.path) {
        return null;
      } else {
        return ob2.path;
      }
    }
  }
}

const saveToDirectory = async (roomId, dataRoomPriviledgeId, file, fileName) => {
  const filePath = getFolderPath(roomId, dataRoomPriviledgeId);
  const time = Date.now();
  let dirPath = path.join(__dirname, `../../${filePath}/${time}_${fileName}`);
  console.log("::::", dirPath)
  return new Promise((resolve, reject) => {
    fs.writeFile(dirPath, file.buffer, {}, (err) => {
      if(err) {
        reject(err);
      }
      resolve({ newFileName: `${fileName}_${time}`, folderPath : dirPath});
    });
  })
}

export default saveToDirectory;