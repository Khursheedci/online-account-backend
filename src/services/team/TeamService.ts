import { TeamRepository } from '../../repositories';

class TeamService {
  private teamRepository: TeamRepository;

  public constructor() {
    this.teamRepository = new TeamRepository();
  }

  public async create(data) {
    return this.teamRepository.create(data);
  }

  public async delete(data) {
    return this.teamRepository.delete(data);
  }

  public async getTeamByQuery(query: any): Promise<any> {
    return this.teamRepository.getTeamByQuery(query);
  }
  public async getTeamById(id: string): Promise<any> {
    return this.teamRepository.getTeamById(id);
  }

  public async getTeamList(options: any): Promise<any> {
    return this.teamRepository.getTeamList({ isAdmin: { $ne: true } }, { password: 0 }, options);
  }

  public async update(data) {
    return this.teamRepository.update(data);
  }
}

export default TeamService;
