import { ReportRepository } from '../../repositories';

class ReportService {
  private reportRepository: ReportRepository;

  public constructor() {
    this.reportRepository = new ReportRepository();
  }

  public async create(data) {
    return this.reportRepository.create(data);
  }

  public async delete(data) {
    return this.reportRepository.delete(data);
  }

  public async getReportByQuery(query: any): Promise<any> {
    return this.reportRepository.getReportByQuery(query);
  }
  public async getReportById(id: string): Promise<any> {
    return this.reportRepository.getReportById(id);
  }

  public async getReportList(options: any): Promise<any> {
    return this.reportRepository.getReportList({ isAdmin: { $ne: true } }, { password: 0 }, options);
  }

  public async update(data) {
    return this.reportRepository.update(data);
  }
}

export default ReportService;
