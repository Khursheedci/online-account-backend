import { PerformanceRepository } from '../../repositories';

class PerformanceService {
  private performanceTargetTargetRepository: PerformanceRepository;

  public constructor() {
    this.performanceTargetTargetRepository = new PerformanceRepository();
  }

  public async create(data) {
    return this.performanceTargetTargetRepository.create(data);
  }

  public async delete(data) {
    return this.performanceTargetTargetRepository.delete(data);
  }

  public async getPerformanceByQuery(query: any): Promise<any> {
    return this.performanceTargetTargetRepository.getPerformanceByQuery(query);
  }
  public async getPerformanceById(id: string): Promise<any> {
    return this.performanceTargetTargetRepository.getPerformanceById(id);
  }

  public async getPerformanceList(options: any): Promise<any> {
    return this.performanceTargetTargetRepository.getPerformanceList({ isAdmin: { $ne: true } }, { password: 0 }, options);
  }

  public async update(data) {
    return this.performanceTargetTargetRepository.update(data);
  }
}

export default PerformanceService;
