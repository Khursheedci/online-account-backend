export interface IAuthProvider {
  active: boolean;
  authentication: IAuthenticationConfig;
  authorization: IAuthorizationConfig;
}

interface IAuthenticationConfig {
  secret: string;
  tokenLiveTime: number
}

interface IAuthorizationConfig {
  getAuthorizations?: boolean;
  allowWhenNoRule?: boolean;
  rules?: IRule[];
}

interface IRule {
  route: string;
  methods: string[];
  allow?: IRuleCondition;
}

interface IRuleCondition {
  isAdmin?: boolean;
  roomPrivlege?: [number];
  reportPrivlege?: number;
  KPIPrivlege?: number;
}
