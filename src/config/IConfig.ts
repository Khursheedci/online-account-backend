export interface IConfig {
  env: string;
  port: string;
  email: {
    from: string;
    host: string;
    password: string;
    port: number;
  };
  corsOrigin: string[];
  mongo: string;
}
